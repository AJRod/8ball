const answers = ['As I see it, yes.', 'Ask again later.', 'Better not tell you now.',
 'Cannot predict now.', 'Concentrate and ask again.', 'Don’t count on it.',
 'It is certain.', 'It is decidedly so.','Most likely.', 'My reply is no.',
 'My sources say no.', 'Outlook not so good.', 'Outlook good.',
 'Reply hazy, try again.',  'Signs point to yes.', 'Very doubtful.',
 'Without a doubt.', 'Yes.', 'Yes – definitely.', 'You may rely on it.'];

    function eightBall() {
        let answer = answers[Math.floor(Math.random() * answers.length)];
        document.getElementById("answer").innerHTML = answer
    };

    let input = document.getElementById("myInput");
    input.addEventListener("keyup", function(event) {
        if (event.keyCode === 13) {
         event.preventDefault();
         document.getElementById("button").click();
        }
      });
     
    
      let i = 0;
     function textColorChange () {
        let text = document.getElementById("heading")
        let textcolor = ["purple", "blue", "green", "red",]
        text.style.color=textcolor[i];
        i = (i+1)%textcolor.length;

     }

     setInterval(textColorChange,500)
